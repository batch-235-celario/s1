const { assert } = require("chai");
const { newUser } = require("../index.js");
const { user } = require("../index.js");

describe("test newUser object", () => {
	it("Assert newUser type is an Object", () => {
		assert.equal(typeof(newUser), "object");
	})

	it("Assert newUser email is type string", () => {
		assert.equal(typeof(newUser.email), "string");
	})

	it("Assert newUser email is not undefined", () =>{
		assert.notEqual(typeof(newUser.email), "undefined");
	})

	it("Assert newUser password is type string", ()=>{
		assert.equal(typeof(newUser.password), "string");
	})

	it("Assert newUser password is at least 16 characters", () => {
		assert.isAtLeast(newUser.password.length, 16);
	})
})

// Activity

describe("test user object", () => {
	it("Assert that the user firstName type is a string", () => {
		assert.equal(typeof(user.firstName), "string");
	})

	it("Assert that the user lastName type is a string", () => {
		assert.equal(typeof(user.lastName), "string");
	})

	it("Assert that the user firstName type is not undefined", () => {
		assert.notEqual(typeof(user.firstName), "undefined");
	})

	it("Assert that the user lastName type is not undefined", () => {
		assert.notEqual(typeof(user.lastName), "undefined");
	})

	it("Assert that the user is atleast 18", () => {
		assert.isAtLeast(user.age, 18);
	})

	it("Assert that the user age type is a number", () => {
		assert.equal(typeof(user.age), "number");
	})

	it("Assert that the user contactNumber is type of string", () => {
		assert.equal(typeof(user.contactNumber), "string");
	})

	it("Assert that the user batch number type is number", () => {
		assert.equal(typeof(user.batchNumber), "number");
	})

	it("Assert that the user batch number is not undefined", () => {
		assert.notEqual(typeof(user.batchNumber), "undefined");
	})

	it("Assert that user password is atleast 16 characters", () => {
		assert.notEqual(user.password.length, 16);
	})		


})